from django.shortcuts import render, redirect
from .models import Category, Product
from cms.models import Banner
from django.contrib.auth.forms import UserCreationForm
from .forms import ProductReviewForm


# Create your views here.

def homepage(request):
    categories = Category.objects.all()
    banners = Banner.objects.all()
    products = Product.objects.order_by('-id')[:10]
    print(products)
    context = {
        'categories': categories,
        'banners': banners,
        'products': products
    }
    return render(request, 'homepage.html', context)


def category_wise(request, category_slug):
    category = Category.objects.get(slug=category_slug)
    categories = Category.objects.all()

    context = {
        'categories': categories,
        'category': category,
        'products': category.product_set.all()
    }

    return render(request, 'category_wise.html', context)


def register(request):
    if request.method == 'POST':
        form = UserCreationForm(request.POST)

        if form.is_valid():
            form.save()
            return redirect('login')
    else:
        form = UserCreationForm()

    context = {
        'form': form
    }

    return render(request, 'registration/register.html', context)


def product_detail(request, product_slug):
    product = Product.objects.get(slug=product_slug)
    categories = Category.objects.all()
    form = ProductReviewForm()

    if request.method == 'POST':
        print(request.POST)
        form = ProductReviewForm(request.POST)

        if form.is_valid():
            review = form.save(commit=False)
            review.product = product
            review.user = request.user
            review.save()

    context = {
        'product': product,
        'categories': categories,
        'form': form
    }

    return render(request, 'product-detail.html', context)
