from django.db import models
from autoslug import AutoSlugField
from django.contrib.auth.models import User


# Create your models here.

class Category(models.Model):
    title = models.CharField(max_length=20)
    slug = AutoSlugField(populate_from='title')

    class Meta:
        verbose_name_plural = 'Categories'

    def __str__(self):
        return self.title


class Product(models.Model):
    title = models.CharField(max_length=20)
    slug = AutoSlugField(populate_from='title')
    category = models.ForeignKey(Category, on_delete=models.CASCADE)
    image = models.ImageField()
    brand_name = models.CharField(max_length=20)
    price = models.FloatField()
    details = models.TextField(blank=True)


class ProductAttribute(models.Model):
    key = models.CharField(max_length=20)
    value = models.CharField(max_length=50)
    product = models.ForeignKey(Product, on_delete=models.CASCADE)


class ProductReview(models.Model):
    product = models.ForeignKey(Product, on_delete=models.CASCADE)
    user = models.ForeignKey(User, on_delete=models.DO_NOTHING)
    rating = models.IntegerField()
    comment = models.TextField()

    def rating_range(self):
        return range(self.rating)


class WishList(models.Model):
    product = models.ForeignKey(Product, on_delete=models.CASCADE)
    user = models.ForeignKey(User, on_delete=models.CASCADE)
