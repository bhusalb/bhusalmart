from django.contrib import admin
from .models import Category, Product, ProductAttribute


class ProductAttributeAdminInline(admin.TabularInline):
    model = ProductAttribute


class ProductAdmin(admin.ModelAdmin):
    inlines = (ProductAttributeAdminInline,)


admin.site.register(Product, ProductAdmin)

# Register your models here.


admin.site.register(Category)
# admin.site.register(Product)