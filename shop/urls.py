from django.urls import path
from .views import homepage, category_wise, register, product_detail

app_name = 'shop'

urlpatterns = [
    path('', homepage, name='homepage'),
    path('category/<str:category_slug>', category_wise, name='category_wise'),
    path('register', register, name='register'),
    path('product/<str:product_slug>', product_detail, name='product_detail')
]
